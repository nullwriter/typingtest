<?php

use App\TypingTestResult;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TypingTestResultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 0; $i < 20; $i++) {
    		$result = new TypingTestResult();
		    $result->anonymous_user = Str::random(20);
		    $result->words_per_minute = rand(5,60);
		    $result->accuracy = rand(10,100);
		    $result->time_in_seconds = rand(10,60);
		    $result->word_count = rand(40,60);
		    $result->save();
	    }
    }
}
