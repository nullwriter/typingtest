<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypingTestResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typing_test_results', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->string('anonymous_user');
	        $table->integer('words_per_minute')->default(0);
	        $table->integer('word_count')->default(0);
	        $table->integer('accuracy')->default(0);
	        $table->integer('time_in_seconds')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('typing_test_results');
    }
}
