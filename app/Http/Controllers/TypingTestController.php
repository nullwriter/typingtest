<?php

namespace App\Http\Controllers;

use App\TypingTestResult;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TypingTestController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
	    return view('typing_test');
    }

    public function save(Request $request)
    {
    	$result = new TypingTestResult();
    	$result->anonymous_user = 'Web-'.Str::random();
	    $result->words_per_minute = $request->words_per_minute;
	    $result->accuracy = $request->accuracy;
	    $result->time_in_seconds = $request->time_in_seconds;
	    $result->word_count = $request->word_count;
	    $result->save();

	    return response()->json([],201);
    }
}
