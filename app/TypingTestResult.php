<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypingTestResult extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'anonymous_user', 'words_per_minute', 'word_count', 'accuracy', 'time_in_seconds'
	];

	/**
	 * Return formatted accuracy.
	 *
	 * @return string
	 */
	public function getAccuracy()
	{
		return $this->accuracy . '%';
	}
}
