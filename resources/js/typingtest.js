
time_setting = 60;
random_setting = 100;
input_text = "Start by typing the text below";
target_setting = $("#output");

type_writer(input_text, target_setting, 0, time_setting, random_setting);

/*
 * typing-test variables
 */
var character_length = 31;
var index = 0;
var letters =  $("#input_text").val();
var started = false;
var current_string = letters.substring(index, index + character_length);
var characters_typed = 0;
var wordcount = 0;
var accuracy_int = 0;

$("html, body").on('click', function(){
    $("#textarea").focus();
});

$("#target").text(current_string);

/**
 * Event for each keystroke when type testing
 */
$(window).keypress(function(evt){

    if(!started){
        // if we just finished, dont auto start
        if (characters_typed > 0) {
            return;
        }

        start();
        started = true;
    }

    evt = evt || window.event;
    var charCode = evt.which || evt.keyCode;
    var charTyped = String.fromCharCode(charCode);

    if(charTyped == letters.charAt(index)){

        if(charTyped == " "){
            wordcount ++;
            $("#wordcount").text(wordcount);
        }

        index ++;
        current_string = letters.substring(index, index + character_length);
        $("#target").text(current_string);
        $("#your-attempt").append(charTyped);
        characters_typed++;

        if(index == letters.length){
            wordcount ++;
            $("#wordcount").text(wordcount);
            $("#timer").text(timer);
            if(timer == 0){
                timer = 1;
            }
            wpm = Math.round(wordcount / (timer / 60));
            $("#wpm").text(wpm);
            stop();
            finished();
            saveResults();
        }
    } else {
        $("#your-attempt").append("<span class='wrong'>" + charTyped + "</span>");
        errors ++;
    }

    calculateAccuracy();
});

var timer = 0;
var wpm = 0;
var errors = 0;
var interval_timer;

$("#btn-play-again").on('click', function(){
    reset();
});

$("#change").on('click', function(){
    $("#input_text").show().focus();
});

$("#pause").on('click', function(){
    stop();
});

$("#input_text").on('change', function(){
    reset();
});

var window_focus;

$(window).focus(function() {
    window_focus = true;
}).blur(function() {
    window_focus = false;
});

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

function type_writer(input, target, current, time, random){
    if(current < input.length){
        current += 2;
        target.text(input.substring(0,current));

        setTimeout(function(){
            type_writer(input, target, current, time, random);
        },time + Math.random()*random);
    }
}

function calculateAccuracy() {
    var acc = ( (characters_typed - errors) / characters_typed ) * 100;

    // dont allow negative percentage
    if (acc < 0) {
        acc = 0
    }

    accuracy_int = acc;
    $('#accuracy').text( Math.trunc(acc) + '%' );
}

function start(){
    interval_timer = setInterval(function(){
        timer ++;
        $("#timer").text(timer);
        wpm = Math.round(wordcount / (timer / 60));
        $("#wpm").text(wpm);
    }, 1000)
}

function stop(){
    clearInterval(interval_timer);
    started = false;
}

function reset(){
    $('#btn-play-again').hide();
    $("#input_text").blur().hide();
    $("#your-attempt").text("");
    index = 0;
    errors = 0;
    clearInterval(interval_timer);
    started = false;
    letters = $("#input_text").val();
    $("#wpm").text("0");
    $("#timer").text("0");
    $("#wordcount").text("0");
    timer = 0;
    wpm = 0;
    characters_typed = 0;
    $('#accuracy').text("0");
    current_string = letters.substring(index, index + character_length);
    $("#target").text(current_string);
}

function finished(){
    alertify.alert("Congratulations!","Words per minute: " + wpm + "<br />Accuracy: " + accuracy_int + "% <br />Words: "
        + wordcount +"<br />Errors: " + errors);
    $('#btn-play-again').show();
}

function saveResults(){
    $.ajax({
        url: "/save",
        method: 'post',
        data: {
            words_per_minute: wpm,
            accuracy: accuracy_int,
            time_in_seconds: timer,
            word_count: wordcount,
        },
        success: function(result){
            console.log("Success: " + result);
        }
    });
}
