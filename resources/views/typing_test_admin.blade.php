<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Typing Test Results</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Cutive+Mono|Roboto:400,900,700' rel='stylesheet' type='text/css'/>

        <!-- Styles -->
        <link type="text/css" href="{{ asset('/css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <h1>Typing Test Results</h1>
        <div class="row">
            <table id="results_table" class="display">
                <thead>
                <tr>
                    <th>Anon User</th>
                    <th>Words / Min</th>
                    <th>Accuracy</th>
                    <th>Time (s)</th>
                    <th>Word Count</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($results as $result)
                        <tr>
                            <td>{{ $result->anonymous_user  }}</td>
                            <td>{{ $result->words_per_minute  }}</td>
                            <td>{{ $result->getAccuracy()  }}</td>
                            <td>{{ $result->time_in_seconds  }}</td>
                            <td>{{ $result->word_count  }}</td>
                            <td>{{ $result->created_at  }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <script type="text/javascript" src="{{ asset('/js/admin/admin.js') }}"></script>
    </body>
</html>
