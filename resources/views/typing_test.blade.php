<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Typing Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Cutive+Mono|Roboto:400,900,700' rel='stylesheet' type='text/css'/>

        <!-- Styles -->
        <link type="text/css" href="{{ asset('/css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <h1 id="output"></h1>
        <div class="content-container">
            <div class="target mono" id="target"></div>
            <div id="your-attempt" class="mono your-attempt" placeholder="Your text will appear here"></div>
        </div>

        <div class="results">
            <h2>Your Score</h2>
            <ul class="stats">
                <li>Timer <span id="timer">0</span></li>
                <li title="Words per minute">Words / Min <span id="wpm">0</span></li>
                <li>Words <span id="wordcount">0</span></li>
                <li>Accuracy <span id="accuracy">0</span></li>
            </ul>
            <button class="btn btn-primary" id="btn-play-again">Play again?</button>
        </div>

        <div>
            <textarea name="" id="input_text" cols="30" rows="10">Red foxes are usually together in pairs or small groups consisting of families, such as a mated pair and their young, or a male with several females having kinship ties. The young of the mated pair remain with their parents to assist in caring for new kits.</textarea>
        </div>
        <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
    </body>
</html>
